define([
    "jquery"
], function($){
    "use strict";
    return function slides(config, element) {
        $(function() {

            var width=940;
            var animationSpeed=500;
            var currentSlide=1;

            var $btn1 = $('#btn1');
            var $btn2 = $('#btn2');
            var $btn3 = $('#btn3');

            var $slider = $('.images');
            var $slideContainer = $slider.find('.slides');
            var $slides = $slideContainer.find('.slide');
            var interval;

            function startSlider() {
                switch(currentSlide){
                    case 1:
                        $btn1.addClass('active');
                        $btn2.removeClass('active');
                        $btn3.removeClass('active');
                        break;
                    case 2:
                        $btn2.addClass('active');
                        $btn1.removeClass('active');
                        $btn3.removeClass('active');
                        break;
                    case 3:
                        $btn3.addClass('active');
                        $btn1.removeClass('active');
                        $btn2.removeClass('active');
                        break;
                }
                interval=setInterval(function() {
                    if(currentSlide === $slides.length){
                        currentSlide = 1;
                        $btn1.addClass('active');
                        $btn2.removeClass('active');
                        $btn3.removeClass('active');
                        $slideContainer.animate({'margin-left': 0});
                    }
                    else if (currentSlide === 2){
                        $btn3.addClass('active');
                        $btn1.removeClass('active');
                        $btn2.removeClass('active');
                        $slideContainer.animate({'margin-left': '-='+width},animationSpeed);
                        currentSlide++;
                    }
                    else{
                        $btn2.addClass('active');
                        $btn1.removeClass('active');
                        $btn3.removeClass('active');
                        $slideContainer.animate({'margin-left': '-='+width},animationSpeed);
                        currentSlide++;
                    }
                },10000);
            }
            function stopSlider() {
                clearInterval(interval);
            }
            startSlider();

            $btn1.on('click', function() {
                currentSlide = 1;
                $slideContainer.animate({'margin-left': 0});
                stopSlider();
                startSlider();
            })
            $btn2.on('click', function() {
                currentSlide = 2;
                $slideContainer.animate({'margin-left': -width});
                stopSlider();
                startSlider();
            })
            $btn3.on('click', function() {
                currentSlide = 3;
                $slideContainer.animate({'margin-left': -width*2});
                stopSlider();
                startSlider();
            })
        });
    }
});