define([
    "jquery"
], function($){
    "use strict";
    return function sticky(config, element) {
        $(function() {
            $(window).scroll(function () {
                if( $(window).scrollTop() > $('.sections.nav-sections').offset().top && !($('.sections.nav-sections').hasClass('sticky'))){
                    $('.sections.nav-sections').addClass('sticky');
                    $('.vertical-menu').css('display','none');

                    //IF WE HOVER "PRODUSE" WHILE THE HEADER IS STICKY THE VERTICAL MENU SHOULD APPEAR
                    $('.produse').mouseenter(function () {
                        $('.vertical-menu').css('display','inline-block');
                    })
                    $('.produse').mouseleave(function () {
                        if( !$('.vertical-menu').is(":hover")) {
                            $('.vertical-menu').css('display', 'none');
                        }
                    })
                    $('.vertical-menu').mouseleave(function () {
                        $('.vertical-menu').css('display','none');
                    })

                    //$('.header.content').offset().top + $('.header.content').outerHeight() MEANS WHEN THE NAV GETS TO THE BOTTOM OF THE UPPER ELEMENT REMOVE STICKY
                } else if ($(window).scrollTop() < $('.header.content').offset().top + $('.header.content').outerHeight() && ($('.sections.nav-sections').hasClass('sticky'))){
                    $('.sections.nav-sections').removeClass('sticky');
                    $('.vertical-menu').css('display','inline-block');
                    $('.produse').off('mouseenter');
                    $('.produse').off('mouseleave');
                    $('.vertical-menu').off('mouseleave');
                }

            })
        });
    }
});