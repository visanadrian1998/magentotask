define([
    "jquery"
], function($){
    "use strict";
    return function navSections() {
        $(function() {
            //WHEN MOUSE HOVERS ON 'ENCICLOPEDIE FITO':
            $('.enciclopedie').mouseenter(function () {
                $('#enciclopedie-onHover').css('display','block');
                $('.enciclopedie-title').addClass("active");
            });
            //WHEN MOUSE LEAVES 'ENCICLOPEDIE FITO':
            $('.enciclopedie').mouseleave(function () {
                //CHECK IF WE ARE HOVERING THE DROPDOWN AND IF NOT:
                if( !$('#enciclopedie-onHover').is(":hover")) {
                    $('#enciclopedie-onHover').css('display', 'none');
                    $('.enciclopedie-title').removeClass("active");
                }
            });
            //WHEN LEAVING THE DROPDOWN STOP DISPLAYING IT
            $('#enciclopedie-onHover').mouseleave(function () {
                $('#enciclopedie-onHover').css('display','none');
                $('.enciclopedie-title').removeClass("active");
            });

            //WHEN HOVERING A ELEMENT FROM THE LIST ADD CLASS FOR IT AND FOR IT'S 'PARTNER' FROM THE RIGHT MENU
            //ALSO REMOVE THE ACTIVE CLASS FROM THE OTHER ELEMENTS
            $('.left-side-item-enciclopedie').mouseenter(function () {
                $(this).addClass('active').siblings().removeClass('active');
                var index = $(this).index();
                $('.right-side-menu-enciclopedie').eq(index).addClass("active").siblings().removeClass('active');
            });

            //THE SAME AS ABOVE BUT FOR 'FORUM FITO'
            $('.forum-fito').mouseenter(function () {
                $('#forum-onHover').css('display','block');
                $('.forum-fito-title').addClass("active");
            });
            $('.forum-fito').mouseleave(function () {
                if( !$('#forum-onHover').is(":hover")) {
                    $('#forum-onHover').css('display', 'none');
                    $('.forum-fito-title').removeClass("active");
                }
            });
            $('#forum-onHover').mouseleave(function () {
                $('#forum-onHover').css('display','none');
                $('.forum-fito-title').removeClass("active");
            });


            $('.left-side-item-forum').mouseenter(function () {
                $(this).addClass('active').siblings().removeClass('active');
                var index = $(this).index();
                $('.right-side-menu-forum').eq(index).addClass("active").siblings().removeClass('active');
            });

            ////////CODE FOR VERTICAL MENU

            //WHEN WE HOVER ON ONE OF THE 3 DIVS THAT COMPOSE THE LIST
            $('.nav-subitems').mouseenter(function () {
                $(this).addClass('active').siblings().removeClass('active');
            });

            //WHEN WE LEAVE ONE OF THE 3 DIVS THAT COMPOSE THE LIST REMOVE THE SHADOW FROM ANY AND EVERY LINK
            $('.nav-subitems').mouseleave(function () {
                $(this).children().children().css('textShadow','');
            });

            $('.vertical-menu-item').mouseenter(function () {
                $(this).children().css('textShadow','0px 0px 1px black').parent().siblings().children().css('textShadow','');
                var index = $(this).attr('id');
                $('.nav-right-submenu').eq(index).addClass("active").siblings().removeClass('active');
            });

            $('.vertical-menu-item').mouseleave(function () {
                //CHECK IF WE ARE HOVERING THE NAV-RIGHT AND IF NOT:
                var index = $(this).attr('id');
                if( !$('.nav-right-submenu').eq(index).is(":hover")){
                    $('.nav-right-submenu').eq(index).removeClass('active');
                }
            });

            $('.nav-right-submenu').mouseleave(function () {
                    $(this).removeClass('active');
            });
        });
    }
});