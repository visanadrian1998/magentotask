var config = {
    map: {
        '*': {
            slides: 'js/slides',
            sticky: 'js/sticky',
            navSections: 'js/navSections'
        }
    }
};